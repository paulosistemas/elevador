package com.Itau;

public class Elevador {

    private int andarAtual;
    private int totalAndares;
    private int capacidadeElevador;
    private int quantidadePessoas;

    public Elevador(int totalAndares, int capacidadeElevador) {
        this.totalAndares = totalAndares;
        this.capacidadeElevador = capacidadeElevador;
    }
    public Elevador() {
        this.totalAndares = 10;
        this.capacidadeElevador = 5;
    }

    public int entraPessoa(){
        this.quantidadePessoas++;
        if (this.quantidadePessoas > capacidadeElevador) {
            System.out.println("Elevador ultrapassou capacidade limite: " + this.quantidadePessoas);
            this.quantidadePessoas--;
        }
        System.out.println("Quantidade Pessoas Atual: " + getQuantidadePessoas());
        return getQuantidadePessoas();
    }

    public int entraPessoa(int quantidadePessoas){
        this.quantidadePessoas+=quantidadePessoas;
        if (this.quantidadePessoas > capacidadeElevador) {
            System.out.println("Elevador ultrapassou capacidade limite: " + this.quantidadePessoas);
            this.quantidadePessoas=capacidadeElevador;
        }
        System.out.println("Quantidade Pessoas Atual: " + getQuantidadePessoas());
        return getQuantidadePessoas();
    }

    public int saiPessoa(){
        this.quantidadePessoas--;
        if (this.quantidadePessoas < 0) {
            System.out.println("Elevador já está vazio: " + this.quantidadePessoas);
            this.setQuantidadePessoas(0);
        }
        System.out.println("Quantidade Pessoas Atual: " + getQuantidadePessoas());
        return getQuantidadePessoas();
    }

    public int saiPessoa(int quantidadePessoas){
        this.quantidadePessoas-=quantidadePessoas;
        if (this.quantidadePessoas < 0) {
            System.out.println("Elevador já está vazio: " + this.quantidadePessoas);
            this.setQuantidadePessoas(0);
        }
        System.out.println("Quantidade Pessoas Atual: " + getQuantidadePessoas());
        return getQuantidadePessoas();
    }

    public int sobeAndar(){
        this.andarAtual++;
        if (this.andarAtual>this.totalAndares){
            System.out.println("Elevador não pode subir mais: " + this.getAndarAtual());
            this.setAndarAtual(getTotalAndares());
        }
        System.out.println("Andar Atual: " + this.getAndarAtual());
        return getAndarAtual();
    }

    public int sobeAndar(int andarAcima){
        this.andarAtual+=andarAcima;
        if (this.andarAtual > getTotalAndares()){
            System.out.println("Elevador não pode subir mais: " + this.getAndarAtual());
            this.setAndarAtual(getTotalAndares());
        }
        System.out.println("Andar Atual: " + this.getAndarAtual());
        return getAndarAtual();
    }
    public int desceAndar(){
        this.andarAtual--;
        if (this.andarAtual < 0){
            System.out.println("Elevador não pode descer mais: " + this.getAndarAtual());
            this.setAndarAtual(0);
        }
        System.out.println("Andar Atual: " + this.getAndarAtual());
        return getAndarAtual();
    }

    public int desceAndar(int andarAbaixo){
        this.andarAtual-= andarAbaixo;
        if (this.andarAtual < 0){
            System.out.println("Elevador não pode descer mais: " + this.getAndarAtual());
            this.setAndarAtual(0);
        }
        System.out.println("Andar Atual: " + this.getAndarAtual());
        return getAndarAtual();
    }

    public void exibirAndar(){
        System.out.println("Andar atual: " + getAndarAtual());
    }

    public void exibirQuantidadePessoas(){
        System.out.println("Quantidade de pessoa dentro do elevador: " + getQuantidadePessoas());
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual(int andarAtual) {
        this.andarAtual = andarAtual;
    }

    public int getTotalAndares() {
        return totalAndares;
    }

    public void setTotalAndares(int totalAndares) {
        this.totalAndares = totalAndares;
    }

    public int getCapacidadeElevador() {
        return capacidadeElevador;
    }

    public void setCapacidadeElevador(int capacidadeElevador) {
        this.capacidadeElevador = capacidadeElevador;
    }

    public int getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(int quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }
}
