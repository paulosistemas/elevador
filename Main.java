package com.Itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Elevador elevador1 = new Elevador();
        Elevador elevador2 = new Elevador(11, 4);
        menu(elevador1);

	// write your code here
    }

    public static void menu(Elevador elevador){
        System.out.print("Digite uma das opções: 1 - Entrar no elevador; 2 - Entrar mais de 1; 3 - Sair do elevador");
        System.out.println("4 - Sair mais de uma pessoa do elevador; 5 - Subir um andar; 6 - Subir mais de um andar");
        System.out.print("7 - Descer um andar; 8 Descer mais de um andar; 9 - Consultar andar atual; 10 - Consultar");
        System.out.println(" quantidade de pessoas dentro do elevador; <enter> - Sair do sistema");
        Scanner inputOpcao = new Scanner(System.in);

        int opcao = inputOpcao.nextInt();

//        while(inputOpcao.hasNext()){
        while(opcao != 11){
            Scanner inputQuantidade = new Scanner(System.in);
            switch(opcao){

                case 1:
                    elevador.entraPessoa();
                    break;

                case 2:
                    System.out.print("Digite a quantidade de pessoas:");
                    elevador.entraPessoa(inputQuantidade.nextInt());
                    break;

                case 3:
                    elevador.saiPessoa();
                    break;

                case 4:
                    System.out.print("Digite a quantidade de pessoas:");
                    elevador.saiPessoa(inputQuantidade.nextInt());
                    break;

                case 5:
                    elevador.sobeAndar();
                    break;

                case 6:
                    System.out.print("Digite quantos andares deseja subir:");
                    elevador.sobeAndar(inputQuantidade.nextInt());
                    break;

                case 7:
                    elevador.desceAndar();
                    break;

                case 8:
                    System.out.print("Digite quantos andares deseja descer:");
                    elevador.desceAndar(inputQuantidade.nextInt());
                    break;

                case 9:
                    elevador.exibirAndar();
                    break;

                case 10:
                    elevador.exibirQuantidadePessoas();
                    break;

                default:
                    System.out.println("Opção inválida");
                    break;
            }

            opcao = inputOpcao.nextInt();
        }
        inputOpcao.close();
    }

}
